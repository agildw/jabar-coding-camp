// soal 1
function jumlah_kata(kalimat) {
    console.log(kalimat.split(' ').length);
}

var kalimat_1 = 'Halo nama saya Agil Dwiki Yudistira';
var kalimat_2 = 'Saya Agil';
jumlah_kata(kalimat_1);
jumlah_kata(kalimat_2);

//soal 2
function next_date(day, month, year) {
    var date = {
        dayInMonth: [null, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
        monthInYear: [null, 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']
    }

    var hasil;

    if (month == 12 && day >= 31) {
        var nextYear = year + 1;
        hasil = 1 + ' ' + date.monthInYear[1] + ' ' + nextYear;
    } else if (month == 2 && day > 28 && year % 4 == 0) {
        hasil = 1 + ' ' + date.monthInYear[3] + year;
    } else if (day >= date.dayInMonth[month]) {
        hasil = 1 + ' ' + date.monthInYear[month + 1] + ' ' + year;
    } else {
        hasil = day + 1 + ' ' + date.monthInYear[month] + ' ' + year
    }
    console.log(hasil);
}

var tanggal = 29;
var bulan = 2;
var tahun = 2021;
next_date(tanggal, bulan, tahun)
