//soal 2
var readBooksPromise = require('./promise.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 },
    { name: 'komik', timeSpent: 1000 }
]

readBooksPromise(10000, books[0]).then(b => readBooksPromise(b, books[1]).then(b => readBooksPromise(b, books[2]).then(b => readBooksPromise(b, books[3]))))