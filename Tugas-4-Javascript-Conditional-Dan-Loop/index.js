//soal 1
var nilai = 76

if (nilai >= 85) {
    console.log('Indeks A');
} else if (nilai >= 75 && nilai < 85) {
    console.log('Indeks B');
} else if (nilai >= 65 && nilai < 75) {
    console.log('Indeks C')
} else if (nilai >= 55 && nilai < 65) {
    console.log('Indeks D')
} else if (nilai < 55) {
    console.log('Indeks E')
} else {
    console.log('Masukkan angka valid')
}

//soal 2
var tanggal = 3;
var bulan = 9;
var tahun = 2003;
var hasil;

switch (bulan) {
    case 1: { hasil = 'Januari'; break; }
    case 2: { hasil = 'Februari'; break; }
    case 3: { hasil = 'Maret'; break; }
    case 4: { hasil = 'April'; break; }
    case 5: { hasil = 'Mei'; break; }
    case 6: { hasil = 'Juni'; break; }
    case 7: { hasil = 'Juli'; break; }
    case 8: { hasil = 'Agustus'; break; }
    case 9: { hasil = 'September'; break; }
    case 10: { hasil = 'Oktober'; break; }
    case 11: { hasil = 'November'; break; }
    case 12: { hasil = 'Desember'; break; }
}

console.log(tanggal + ' ' + hasil + ' ' + tahun)


//soal 3
var n = 5;

for (var i = "#"; i.length <= n; i += '#') {
    console.log(i);
}

//soal 4
var m = 7;
var j = 0;
let text;
var separator = '';
for (var i = 1; i <= m; i++) {
    switch (i - j) {
        case 1: text = 'I love programming'; break;
        case 2: text = 'I love javascript'; break;
        case 3: text = 'I love VueJS'; j += 3; break;

    }console.log(i + ' - ' + text)

    if (i % 3 === 0) {
        for (var a = 0; a < i; a++) {
            separator += '=';
        }
        console.log(separator)
        separator = '';
    }


}