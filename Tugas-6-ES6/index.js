//soal 1
const persegiPanjang = (a, b) => {
    let keliling = (2 * a) + (2 * b)
    let luas = a * b;
    console.log(keliling, luas)
}

persegiPanjang(6, 4)

// soal 2

const newFunction = (firstName, lastName) => {
    return {
        firstName: firstName,
        lastName: lastName,
        fullName: () => {
            console.log(firstName + ' ' + lastName)
        }
    }

}
//Driver Code
newFunction('Agil', 'Yudistira').fullName();

//soal 3
const newObject = {
    firstName: "Agil",
    lastName: "Dwiki Yudistira",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}


const { firstName, lastName, address, hobby } = newObject;
console.log(firstName, lastName, address, hobby);

//soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)

//soal  5
const planet = "earth"
const view = "glass"

var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`
//Driver Code
console.log(before)