export const MembersComponent = {
    template: `
    <tr>
    <td>
           <img :src="contents.photo_profile ? baseUrl + contents.photo_profile : 'https://dummyimage.com/300x200/000/fff&text=404'" alt="photo profile" width="125">
           </td>
           <td>
               ID : {{contents.id}}<br />
               Nama : {{contents.name}}<br />
               Address : {{contents.address}} <br />
               No. HP : {{contents.no_hp}}<br />
           </td>
           <td>
                <button v-on:click="$emit('is-update',contents)" style="width: 139px;">Edit</button><br />
                <button v-on:click="$emit('is-delete',contents.id)" style="width: 139px;">Delete</button> <br />
                <button v-on:click="$emit('is-uploaded', contents)" style="width: 139px;">Add photo</button>   
       </td>
       </tr>
`, data() {
        return {
            baseUrl: 'http://demo-api-vue.sanbercloud.com'
        }
    },
    props: ['contents']
}

export const TableComponent = {
    template: `
    <table border=1>
    <slot></slot>
    </table>
`,

}